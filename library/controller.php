<?php
$action = empty($_GET['action']) ? 'home' : $_GET['action'];
$page = null;
$title = null;

switch ($action) {
	case 'home':
        require_once './action/home.php';
		break;
	case 'about':
		$page = './viwes/about.php';
        $title = "О нас";
		break;
    case 'article':
        require_once './action/article.php';
        break;
    case 'store':
        $page = './viwes/store.php';
        $title = "Магазин";
        break;
    case 'cart':
        $page = './viwes/cart.php';
        $title = "Корзина";
        break;
	default:
		$page = './viwes/404.php';
		break;
}