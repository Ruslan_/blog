<div class="row mt-5">


    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="https://cdn1.savepice.ru/uploads/2018/3/14/f15055f0a76af3e7881f53b87611e965-full.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Форма <span class="price">950</span> грн</h5>
                <p class="card-text">Оригинальная форма футбольного клуба Ювентус.</p>
                <a href="#" class="btn btn-primary">В корзину</a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="https://cdn1.savepice.ru/uploads/2018/3/14/7a3eff3c29e9c5abbe1a42967e7bce10-full.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Сумка <span class="price">1500</span> грн</h5>
                <p class="card-text">Сумка с логотипом Ювентуса из качественного материала.</p>
                <a href="#" class="btn btn-primary">В корзину</a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="https://cdn1.savepice.ru/uploads/2018/3/14/365c80b14126628547572c22736fb0be-full.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Ремень <span class="price">400</span> грн</h5>
                <p class="card-text">Отличный кожаный ремень с символикой футбольного клуба Ювентус.</p>
                <a href="#" class="btn btn-primary">В корзину</a>
            </div>
        </div>
    </div>
</div>
<div class="row mt-5">
    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="https://cdn1.savepice.ru/uploads/2018/3/14/c0b2741d03799fa91359be0b6396a6ad-full.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Кресло-мешок <span class="price">2200</span> грн</h5>
                <p class="card-text">Мягкое кресло для комфортного просмотра футбольных матчей.</p>
                <a href="#" class="btn btn-primary">В корзину</a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="https://cdn1.savepice.ru/uploads/2018/3/14/a8def9f624ba863844433f15dd06853e-full.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Геймпад <span class="price">670</span> грн</h5>
                <p class="card-text">С этим великолепным геймпадом побеждать в Fifa станет намного легче и приятнее.</p>
                <a href="#" class="btn btn-primary">В корзину</a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="https://cdn1.savepice.ru/uploads/2018/3/14/3c60006ee55e20226ae625dada3deb15-full.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Запанки <span class="price">180</span> грн</h5>
                <p class="card-text">Стильные запанки с логотипом Ювентуса станут отличным аксессуаром для самого преданного фаната.</p>
                <a href="#" class="btn btn-primary">В корзину</a>
            </div>
        </div>
    </div>
</div>